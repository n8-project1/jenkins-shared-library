#!/usr/bin/env groovy
package com.example

class Docker implements Serializable {

    def script

    Docker (script) {           //adding this will make these methods exposed globally. We have to append with
                                //"script." below
        this.script = script
    }

    def buildDockerImage(String imageName) {       //then we take the function contents out of the buildImage.groovy
        script.echo "building the docker image..."
        script.sh "docker build -t $imageName ."
    }

    def dockerLogin(){
        script.withCredentials([usernamePassword(credentialsId: 'docker-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
            script.sh "echo '${script.PASS}' | docker login -u '${script.USER}' --password-stdin"
        }
    }

    def dockerPush(String imageName) {
        script.sh "docker push $imageName"
    }

}
