#!/usr/bin/env groovy 
def call() {

    withCredentials([usernamePassword(credentialsId: 'docker-credentials', usernameVariable: 'USER', passwordVariable: 'PASS')]){
        sh "docker build -t n8xx/demo-app:${IMAGE_NAME} ."
        sh 'echo $PASS | docker login -u $USER --password-stdin'
        sh "docker push n8xx/demo-app:${IMAGE_NAME}"
    }

}
