#!/usr/bin/env groovy 

def call() {

        // enter app directory, because thats where package.json and tests are located
    dir("app") {
        
        // install all dependencies needed for running tests
    sh "npm install"
    sh "npm run test"

    }
       
}
